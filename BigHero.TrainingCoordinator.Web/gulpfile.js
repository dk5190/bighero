﻿/// <binding BeforeBuild='bundle' />
var project = require("./project.json");
var gulp = require("gulp");

var paths = {
    scripts: project.webroot + "/scripts/",
    app: project.webroot + "/scripts/app/"
};

var concat = require("gulp-concat"),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify");

gulp.task("bundle", function () {
    return gulp.src([
            paths.app + "bootstrap.js",
            paths.app + "controllers/*.js"])
        .pipe(concat("trainingCoordinator.js"))
        .pipe(gulp.dest(paths.scripts))
        .pipe(rename("trainingCoordinator.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts));
});
