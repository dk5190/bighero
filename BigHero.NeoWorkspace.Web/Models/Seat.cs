﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigHero.NeoWorkspace.Web.Models
{
    public class Seat
    {
        public string SeatCode { get; set; }
        public string CurrentStaffCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
