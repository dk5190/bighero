﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BigHero.NeoWorkspace.Web.Models
{
    public class Staff
    {
        public string StaffCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CurrentSeatCode { get; set; }
    }
}
