(function () {
    window.neoWorkspace = {
        app: angular.module("neoWorkspace", ['ngRoute'])
    };

    var app = window.neoWorkspace.app;
    app.run(['$rootScope', function($rootScope) {
	$rootScope.applicationText = 'Neo Workspace';
    }]);


})();



(function (app) {
    app.controller("homeController", ["$rootScope", "$scope", function ($rootScope, $scope) {
	
    }]);
})(app = window.neoWorkspace.app);

(function (app) {
    app.controller("workspaceController", ["$scope", function ($scope) {
        $scope.welcomeText = "babababa";
    }]);
})(app = window.neoWorkspace.app);

(function(app) {
    app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	    .when('/', {
		templateUrl: 'views/home.html',
		controller: 'homeController'
	    })
	    .otherwise({
		redirectTo: '/'
	    });
    }]);
})(app = window.neoWorkspace.app);
