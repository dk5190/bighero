﻿(function () {
    window.neoWorkspace = {
        app: angular.module("neoWorkspace", ['ngRoute'])
    };

    var app = window.neoWorkspace.app;
    app.run(['$rootScope', function($rootScope) {
	$rootScope.applicationText = 'Neo Workspace';
    }]);


})();


