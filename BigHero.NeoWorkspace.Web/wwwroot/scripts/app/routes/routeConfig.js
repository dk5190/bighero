(function(app) {
    app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	    .when('/', {
		templateUrl: 'views/home.html',
		controller: 'homeController'
	    })
	    .otherwise({
		redirectTo: '/'
	    });
    }]);
})(app = window.neoWorkspace.app);
