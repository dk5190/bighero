function Desk(posX, posY) {
    this.x = posX;
    this.y = posY;
    this.color = "brown";
    this.width = 50;
    this.height = 10;
    this.velX = 0;
    this.velY = 0;
    this.accelX = 0;
    this.accelY = 0;
}

Desk.prototype.render = function(context) {
    context.fillStyle = this.color;
    context.fillRect(this.x, this.y, this.width, this.height);
};

window.onload = function() {

var drawingArea = document.getElementById('drawingArea');
var context = drawingArea.getContext('2d');

document.getElementById('addDeskButton').onclick = function() {
    var desk = new Desk(10, 20);
    desk.render(context);
};

};

