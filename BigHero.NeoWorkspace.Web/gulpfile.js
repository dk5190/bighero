﻿/// <binding BeforeBuild='bundle' />
var project = require("./project.json");
var gulp = require("gulp");
var sass = require('gulp-sass');

var paths = {
    css: project.webroot + "/styles/",
    scripts: project.webroot + "/scripts/",
    app: project.webroot + "/scripts/app/"
};

var concat = require("gulp-concat"),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify");

gulp.task("bundle", function () {
    gulp.src(paths.css + "*.scss")
	.pipe(sass().on('error', sass.logError))
	.pipe(concat("neoWorkspace.css"))
	.pipe(gulp.dest(paths.css))

    return gulp.src([
            paths.app + "bootstrap.js",
            paths.app + "controllers/*.js",
	    paths.app + "routes/*.js"])
        .pipe(concat("neoWorkspace.js"))
        .pipe(gulp.dest(paths.scripts))
        .pipe(rename("neoWorkspace.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts));
});
